package my.domain;

import java.util.List;

public class Cell {
  private boolean alive;
  private final Channel<Boolean> tickChannel;
  private final Channel<Boolean> resultChannel;
  private final List<Channel<Boolean>> inChannels;
  private final List<Channel<Boolean>> outChannels;

  Cell(CellOptions options) {
    this.alive = options.alive();
    this.tickChannel = options.tickChannel();
    this.resultChannel = options.resultChannel();
    this.inChannels = options.inChannels();
    this.outChannels = options.outChannels();
  }

  void start() {
    Thread.startVirtualThread(this::run);
  }

  private void run() {
    while (true) {
      // wait for tick stimulus
      tickChannel.take();
      // announce liveness to neighbors
      outChannels.forEach(ch -> ch.put(alive));
      // receive liveness from neighbors
      int neighbors = inChannels.stream().map(Channel::take).mapToInt(b -> b ? 1 : 0).sum();
      // calculate next state based on game of life rules
      alive = nextState(alive, neighbors);
      // announce resulting next state
      resultChannel.put(alive);
    }
  }

  private static boolean nextState(boolean alive, int neighbors) {
    if (alive) {
      return neighbors == 2 || neighbors == 3;
    }
    return neighbors == 3;
  }
}
